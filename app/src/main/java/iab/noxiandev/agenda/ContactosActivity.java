package iab.noxiandev.agenda;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ContactosActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactos);

        ArrayList<String> nom = getIntent().getStringArrayListExtra("nombre");
        ArrayList<String> corre = getIntent().getStringArrayListExtra("correo");
        ArrayList<String> telefo = getIntent().getStringArrayListExtra("telefono");

        // -- listview donde se mostraran los contactos
        ListView lista = (ListView) findViewById(R.id.lista_contactos);
        // -- adapter, sirve para mostrar algo en la list view en este caso se le pasa el arreglo nom (nombres)
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, nom);
        lista.setAdapter(adapter); // -- se hace un set del adapter en la listview

        // -- método al seleccionar un item de la lista
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // -- obtenemos la posicion en el array del nombre seleccionado en la ListView
                String posicion = nom.get(position);
                // -- intent para pasar los arreglos y la posición al siguiente activity
                Intent i = new Intent(ContactosActivity.this, VerContactoActivity.class);
                // -- arreglos
                i.putStringArrayListExtra( "nom", nom);
                i.putStringArrayListExtra( "email",corre);
                i.putStringArrayListExtra( "tel",telefo);
                // -- posición
                i.putExtra("id", posicion);
                startActivity(i);
            }

        });


        // -- boton volver
        Button volver = (Button) findViewById(R.id.btn_volver);
        volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // -- volver al Main
                Intent i = new Intent(v.getContext(),MainActivity.class);
                startActivity(i);
            }
        });

    }
}