package iab.noxiandev.agenda;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class VerContactoActivity extends AppCompatActivity {


    private TextView verNom;
    private TextView verCorreo;
    private TextView verTel;

    private final int PHONE_CALL_CODE = 100;
    private final int CAMERA_CALL_CODE = 120;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_contacto);

        ArrayList<String> nom = getIntent().getStringArrayListExtra("nom");
        ArrayList<String> corre = getIntent().getStringArrayListExtra("email");
        ArrayList<String> telefo = getIntent().getStringArrayListExtra("tel");
        Integer id = getIntent().getExtras().getInt("id");

        verNom = (TextView) findViewById(R.id.txt_verNombre);
        verCorreo = (TextView) findViewById(R.id.txt_verCorreo);
        verTel = (TextView) findViewById(R.id.txt_verTelefono);

        verNom.setText(nom.get(id));
        verCorreo.setText(corre.get(id));
        verTel.setText(telefo.get(id));

        ImageButton llamar = (ImageButton) findViewById(R.id.btn_llamar);
        llamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num = verTel.getText().toString();
                if(num != null){
                    // -- Comprobar versión actual de android
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PHONE_CALL_CODE);
                    }else {
                        versionesAnteriores(num);
                    }
                }
            }
        });

        // -- boton volver
        Button volver = (Button) findViewById(R.id.btn_volverLista);
        volver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // -- volver al Main
                Intent i = new Intent(v.getContext(),ContactosActivity.class);
                startActivity(i);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result = grantResults[0];
                if (permission.equals(Manifest.permission.CALL_PHONE)){
                    // -- COMPROBAR SI HA SIDO ACEPTADO O DENEGADO LA PETICION DEL PERMISO
                    if (result == PackageManager.PERMISSION_GRANTED){
                        // -- concedido
                        String phoneNumber = verTel.getText().toString();
                        Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+phoneNumber));
                        if(ActivityCompat.checkSelfPermission(this,Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)return;
                            startActivity(llamada);
                        Toast.makeText(this, "App" , Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(this,"No aceptaste el permiso", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    // Versión de teléfono
    public void versionesAnteriores(String num){
        Intent iLlamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+num));
        if (verificaPermisos(Manifest.permission.CALL_PHONE)){
            startActivity(iLlamada);
        }else{
            Toast.makeText(VerContactoActivity.this, "Configura los permisos", Toast.LENGTH_SHORT);
        }
    }

    // Comprobar si esta en el manifest
    private boolean verificaPermisos(String permiso){
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return  resultado == PackageManager.PERMISSION_GRANTED;
    }


}