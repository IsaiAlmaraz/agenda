package iab.noxiandev.agenda;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    String nom, email, tel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // -- declaración de los ArrayList
        ArrayList<String> contactoNombres = new ArrayList<>();
        ArrayList<String> contactoCorreos = new ArrayList<>();
        ArrayList<String> contactoTelefonos = new ArrayList<>();

        // txt nombre
        TextView nombre = (TextView) findViewById(R.id.txt_Nombre);
        TextView correo = (TextView) findViewById(R.id.txt_Correo);
        TextView telefono = (TextView) findViewById(R.id.txt_Telefono);

        // -- botón crear nuevo contacto
        Button crear =  (Button) findViewById(R.id.btn_guardar);
        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // -- se obtiene el valor de los TextView
                nom = nombre.getText().toString();
                email = correo.getText().toString();
                tel = telefono.getText().toString();


                // -- se añaden los valores al ArrayList correpondiente
                contactoNombres.add(nom);
                contactoCorreos.add(email);
                contactoTelefonos.add(tel);

                Toast.makeText(getApplicationContext(), "Contacto guardado", Toast.LENGTH_LONG).show();

            }
        });

        // -- ver lista de contactos
        Button verLista = (Button) findViewById(R.id.btn_verLista);
        verLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // -- mandar datos a un segundo activity y se vizualizan en un ListView
                Intent i = new Intent(v.getContext(),ContactosActivity.class);
                i.putStringArrayListExtra( "nombre",contactoNombres);
                i.putStringArrayListExtra( "correo",contactoCorreos);
                i.putStringArrayListExtra( "telefono",contactoTelefonos);

                startActivity(i);
            }
        });

        // -- cancelar registro
        Button cancelar = (Button) findViewById(R.id.btn_cancelar);
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nombre.setText("");
                correo.setText("");
                telefono.setText("");
            }
        });

    }
}